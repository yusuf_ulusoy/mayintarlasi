import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // Default mayın tarlası bilgileri
    grid: 5,
    gridMap: [],
    // Default oyuncu bilgileri
    playerName: '',
    playerScore: 0,
    // Default oyun durumu
    gameStatus: false,
    isPlay: false,
    // Top 10
    players: [],
    // Oyunu başlatmak için gerekli modal durumu
    modal: false
  },
  getters: {
    createMap(state) {
      /**
       * Mayın tarlası için toplam kutu sayısı.
       */
      let matrix = Math.pow(state.grid, 2);
      /**
       * Seçilen alan için eşsiz mayın indisleri.
       */
      let mines = [];
      while (mines.length < state.grid) {
        let rnd = Math.round(Math.random() * matrix);
        if (mines.indexOf(rnd) == -1) {
          mines.push(rnd);
        }
      }
      /**
       * Mayın tarlası için gerekli alanı oluşturur.
       */
      state.gridMap = [];
      for (let i = 0; i < matrix; i++) {
        state.gridMap.push({
          // İlgili alan mayın mı değil mi?
          isMine: mines.indexOf(i) != -1,
          // İlgili alan tıklanmış mı?
          isClicked: false
        });
      }
    }
  },
  mutations: {
    changePlayer(state, playerName) {
      /**
       * Eğer oyuncu score elde etmiş ise oyuncuyu kayıt eder.
       */
      if (state.gameStatus && state.playerScore > 0) {
        /**
         * Oyuncu daha önce var mı diye kontrol sağlar.
         */
        var findPlayer = state.players.findIndex(player => player.name == playerName)
        if (findPlayer != -1) {
          /**
           * Oyuncu daha önce var ise score oranını günceller.
           */
          state.players[findPlayer].score = state.playerScore;
        } else {
          /**
           * Oyuncu daha önce yok ise yeni oyuncu olarak ekler.
           */
          state.players.push({
            name: playerName,
            score: state.playerScore
          })
        }
        /**
         * Oyun her yeniden başlatıldığında Top 10 listesini score oranına göre sıralar.
         */
        state.players = state.players.sort((a,b) => parseFloat(b.score) - parseFloat(a.score));
        /**
         * Top 10 listesinden küme düşenleri gönderelim :)
         */
        state.players = state.players.slice(0, 10)
      }
      /**
       * Oyuncu adı değişmişs state'i ise günceller.
       */
      state.playerName = playerName;
    },
    changeGrid(state, grid) {
      /**
       * Yeni oyun için seçilen gridi günceller.
       */
      state.grid = grid;
    },
    startGame(state, status) {
      /**
       * Oyunu başlatır.
       */
      state.gameStatus = status;
      state.isPlay = status;
      state.playerScore = 0;
    },
    endGame(state) {
      state.modal = true;
    },
  },
  modules: {
  }
})
